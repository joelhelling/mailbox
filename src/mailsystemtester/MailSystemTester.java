/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailsystemtester;

/**
 *
 * @author 
 */
import java.util.Scanner;

/**
   This program tests the mail system. 
*/
public class MailSystemTester
{
   public static void main(String[] args)
   {
      MailSystem system = new MailSystem(MAILBOX_COUNT);
      Scanner console = new Scanner(System.in);
      Telephone p = new Telephone(console);
      Connection c = new Connection(system, p);
      p.run(c);
   }

   private static final int MAILBOX_COUNT = 20;
}
